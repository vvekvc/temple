# For Pagination in ListAPI
from rest_framework.pagination import (
    LimitOffsetPagination,
    PageNumberPagination
)


# Custom LimitOffset pagination
class TempleLimitOffsetPagination(LimitOffsetPagination):
    default_limit = 2
    max_limit = 10


# Custom PageNumberPagination

class TemplePageNumberPagination(PageNumberPagination):
    page_size = 2